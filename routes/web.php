<?php

use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\PlaceController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\UserController;
use App\Http\Livewire\Filter;
use App\Models\Categorie;
use App\Models\Place;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home', ['places' => Place::all(), 'categories' => Categorie::all()]);
})->name('home');

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth.banned', 'auth', 'auth.restricted'])->name('dashboard');



// PLACES
Route::get(
    '/places',
    [PlaceController::class, 'index'],
    // [PlaceController::class, 'viewByCat']
)->name('places');

Route::get(
    'components.Modal.addPlace',
    [PlaceController::class, 'showCategories']
)->name('showCategories');

Route::get(
    '/places/categories?category={id}',
    [PlaceController::class, 'viewByCat']
)->name('filterByCat');

Route::post(
    '/postPlace',
    [PlaceController::class, 'postPlace'] // nom de la fonction
)->middleware(['auth', 'checkPlaces'])->name('postPlace'); // nom de la route (qui sert à pointer vers une action d'un controller)

Route::get(
    '/place/{id}',
    [PlaceController::class, 'showPlace']
)->name('showPlace');

Route::get(
    '/categorie',
    [PlaceController::class, 'listCategories']
)->middleware(['auth'])->name('listCategories');

Route::post(
    '/updatePlace/{id}',
    [PlaceController::class, 'savePlace']
)->middleware(['auth'])->name('savePlace');

Route::get(
    '/deletePlace/{id}',
    [PlaceController::class, 'deletePlace']
)->middleware(['auth'])->name('deletePlace');

Route::get(
    '/updatePlace/{id}',
    [PlaceController::class, 'updatePlace']
)->middleware(['auth'])->name('updatePlace');

Route::post(
    '/',
    [PlaceController::class, 'addPlace']
)->middleware(['auth'])->name('addPlace');



// USERS
Route::get(
    '/user/{id}',
    [UserController::class, 'showUser']
)->middleware(['auth'])->name('showUser');

Route::get(
    '/updateInfos/{id}',
    [UserController::class, 'updateInfos']
)->middleware(['auth'])->name('updateInfos');

Route::post(
    '/updateInfos/{id}',
    [UserController::class, 'saveInfos']
)->middleware(['auth'])->name('saveInfos');

Route::get(
    '/destroy',
    [UserController::class, 'destroy']
)->middleware(['auth'])->name('destroy');



// COMMENTS
Route::post(
    '/postComment/{id}',
    [CommentController::class, 'postComment']
)->middleware(['auth'])->name('postComment');

Route::get(
    '/deleteComment/{id}',
    [CommentController::class, 'deleteComment']
)->middleware(['auth'])->name('deleteComment');

Route::get(
    '/updateComment/{id}/{idPlace}',
    [CommentController::class, 'updateComment']
)->middleware(['auth'])->name('updateComment');

Route::post(
    '/updateComment/{id}/{idPlace}',
    [CommentController::class, 'saveComment']
)->middleware(['auth'])->name('saveComment');


//ADMIN ROUTES
Route::prefix('admin')->middleware(['auth.banned', 'auth', 'auth.restricted'])
    ->name('admin.')->group(function () {
        Route::resource('/users', AdminUserController::class);
    });

require __DIR__ . '/auth.php';
