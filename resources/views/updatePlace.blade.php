<style>
    .openModal {
        display: none;
    }
</style>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Mettre à jour un lieu') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    Mettre à jour {{ $place->name }}<br>
                    <form action="{{ route('updatePlace', $place->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mt-2">
                            @csrf
                            <div class="col-span-6 sm:col-span-3 mb-4">
                                <label for="first-name" class="block text-sm font-medium text-gray-700">Nom du
                                    lieu</label>
                                <input type="text" name="name" id="first-name" value="{{ $place->name }}" autocomplete="given-name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            </div>

                            <div class="col-span-6 sm:col-span-3 mb-4">
                                <label for="last-name" class="block text-sm font-medium text-gray-700">Adresse</label>
                                <input type="text" name="address" id="address" value="{{ $place->address }}" autocomplete="address" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            </div>


                            <div class="col-span-6 sm:col-span-3 mb-4">
                                <label for="last-name" class="block text-sm font-medium text-gray-700">Téléphone
                                    (optionnel)</label>
                                @if ($place->phone !== 'NULL')
                                <input type="text" name="phone" id="phone" value="{{ $place->phone }}" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @else
                                <input type="text" name="phone" id="phone" value="" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @endif
                            </div>

                            <div class="col-span-6 sm:col-span-3 mb-4">
                                <label for="last-name" class="block text-sm font-medium text-gray-700">Site web
                                    (optionnel)</label>
                                @if ($place->website !== 'NULL')
                                <input type="text" name="website" id="website" value="{{ $place->website }}" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @else
                                <input type="text" name="website" id="website" value="" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                @endif
                            </div>

                            <div class="mb-4">
                                <label for="desc" class="block text-sm font-medium text-gray-700"> Commentaire </label>
                                <div class="mt-1">
                                    <textarea id="desc" name="description" rows="3" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md">{{ $place->description }}</textarea>
                                </div>
                            </div>

                            <div class="col-span-6 sm:col-span-3 lg:col-span-2 mb-4">
                                <label for="price" class="block text-sm font-medium text-gray-700">Prix moyen</label>
                                <input type="number" name="price" placeholder="0.00" value="{{ $place->avg_price }}" id="price" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            </div>

                            <div class="col-span-6 sm:col-span-3 mb-4">
                                <label for="note" class="block text-sm font-medium text-gray-700">Note</label>
                                <select name="note" id="note" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                    @for ($i = 1; $i <= 5; $i++) <option value="{{ $i }}" {{ $place->note === $i ? 'selected' : '' }}>
                                        {{ $i }}</option>
                                        @endfor
                                </select>
                            </div>

                            <div class="col-span-6 sm:col-span-3 mb-4">
                                <label for="categories" class="block text-sm font-medium text-gray-700">Catégorie</label>
                                <select name="categories" id="categories" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">

                                    @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }} </option>
                                    @endforeach
                                </select>
                            </div>
                            @csrf

                            <div class="mb-4">
                                <label class="block text-sm font-medium text-gray-700"> Image</label>
                                <div class="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md" id="previewFile" style=" background-repeat: no-repeat; background-size: cover; background-position: center;position:relative;aspect-ratio: 16/9; max-width: 300px;align-items:center; background-image:url({{ asset('/storage/images/' . $place->image) }})">
                                    <svg class="deleteImage" style="position: absolute; width: 24px; top: 10px; right: 10px; background-color: white;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18">
                                        <g id="a" />
                                        <g id="b">
                                            <g id="c">
                                                <path d="M18,1c0-.48-.38-1-1-1H1C.38,0,0,.52,0,1v16c0,.62,.52,1,1,1H17c.48,0,1-.38,1-1V1ZM9.01,7.93l2.72-2.72c.15-.15,.34-.22,.53-.22,.4,0,.75,.32,.75,.75,0,.19-.07,.38-.22,.53l-2.72,2.72,2.73,2.73c.15,.15,.22,.34,.22,.53,0,.43-.35,.75-.75,.75-.19,0-.38-.07-.53-.22l-2.73-2.73-2.73,2.73c-.15,.15-.34,.22-.53,.22-.4,0-.75-.32-.75-.75,0-.19,.07-.38,.22-.53l2.73-2.73-2.72-2.72c-.15-.15-.22-.34-.22-.53,0-.43,.35-.75,.75-.75,.19,0,.38,.07,.53,.22l2.72,2.72Z" />
                                            </g>
                                        </g>
                                    </svg>
                                    <div class="space-y-1 text-center inputImage invisible">
                                        <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                                            <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                        </svg>
                                        <div class="flex text-sm text-gray-600 ">
                                            <label for="image" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                <span>Ajouter une image</span>
                                                <input id="image" name="image" accept="image/png, image/jpeg" type="file" class="sr-only">
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="submit" value="Mettre à jour ce lieu" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4
            py-2 text-base font-medium text-white bg-amber-800 hover:bg-amber-900 focus:outline-none focus:ring-2
            focus:ring-offset-2 focus:ring-red-500 sm:ml-auto sm:w-auto sm:text-sm" />
                    </form>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
