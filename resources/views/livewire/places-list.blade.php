<div>
    <div class="flex item-stretch justify-between bg-amber-800 w-full px-4 py-4 gap-12">
        <div>
            <label class="text-white">Filtrer par :</label>
            <select id="byCat" wire:model="byCat" class="mx-2 rounded-lg">
                <option value="">Catégories</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="grow">
            <div class="flex items-center text-white">
                <label for="query" class="">Rechercher</label>
                <input type="search" wire:model="query" id="query" placeholder="Ex: John Doe"
                    class="mx-2 p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:border-l-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-black dark:focus:border-blue-500 text-black">
            </div>
        </div>

        <div>
            <label class="text-white" for="per_page">Lieux par page : </label>
            <select id="per_page" wire:model.lazy="perPage" class="rounded-lg mx-2">
                @for ($i = 6; $i <= 36; $i += 6)
                    <option value="{{ $i }}">{{ $i }}</option>
                @endfor
            </select>
        </div>
    </div>

    <div class="grid px-4 py-4 flex-wrap grid-cols-3 gap-3">
        @foreach ($places as $place)
            <a href="{{ route('showPlace', ['id' => $place->id]) }}">
                <div class="flex flex-col justify-center items-center" style="background-color:#E5E7EB">
                    <img src="{{ asset('/storage/images/' . $place->image) }}" alt="{{ $place->name }}"
                        style="aspect-ratio:16/9;object-fit:cover">
                    <p>{{ $place->name }}</p>
                    <strong>
                        @foreach ($categories as $category)
                            @if ($category->id == $place->cat_id)
                                {{ $category->name }}
                            @endif
                        @endforeach
                    </strong>
                </div>
            </a>
        @endforeach
    </div>


    <div class="px-4 py-4">
        {{ $places->links() }}
    </div>

</div>
