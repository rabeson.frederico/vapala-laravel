<div class="relative z-10" aria-labelledby="modal-title" role="dialog" aria-modal="true">
    <div class="fixed z-10 inset-0 invisible overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true" id="interestModalDeletePlace{{$place->id }}">
        <div class="fixed z-10 inset-0 overflow-y-auto">
            <div class="flex items-end sm:items-center justify-center min-h-full p-4 text-center sm:p-0">
                <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
                <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>
                <form action="{{ route('deletePlace', $place->id) }}" method="GET" enctype="multipart/form-data">
                    <div class="relative bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:max-w-lg sm:w-full">
                        <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                            <div class="sm:flex sm:items-start">
                                <div class="m-7 text-center sm:text-left">
                                    <h2 class="text-2xl mb-7 leading-normal font-medium text-gray-900" id="modal-title">
                                        Êtes-vous sûre de vouloir supprimer ce lieu ?</h2>
                                </div>
                            </div>
                        </div>
                        <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                            <input type="submit" value="Supprimer" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4
           py-2 text-base font-medium text-white bg-amber-800 hover:bg-amber-900 focus:outline-none focus:ring-2
           focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm" />
                            <button type="button" class="closeModalPlace{{$place->id }} mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                                Annuler
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.openModalDeletePlace{{$place->id }}').on('click', function(e) {
            $('#interestModalDeletePlace{{$place->id}}').removeClass('invisible');
        });
        $('.closeModalPlace{{$place->id }}').on('click', function(e) {
            $('#interestModalDeletePlace{{$place->id }}').addClass('invisible');
        });
    });
</script>
