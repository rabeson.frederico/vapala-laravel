<i>
    <button type="button"
        class="focus:outline-none openModale{{ $comment->id }} text-white text-sm py-2.5 px-5 ml-2 rounded-md bg-amber-800">Modifier</button>

    <div id="ModalEdit{{ $comment->id }}" class="relative z-10" aria-labelledby="modal-title" role="dialog"
        aria-modal="true">
        <div class="fixed z-10 inset-0 invisible overflow-y-auto" aria-labelledby="modal-title" role="dialog"
            aria-modal="true" id="interestModale{{ $comment->id }}">
            <div class="fixed z-10 inset-0 overflow-y-auto">
                <div class="flex items-end sm:items-center justify-center min-h-full p-4 text-center sm:p-0">
                    <div class="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" aria-hidden="true"></div>
                    <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">​</span>
                    <form action="{{ route('updateComment', [$comment->id, $place]) }}" method="POST"
                        enctype="multipart/form-data">
                        <div
                            class="relative bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:max-w-lg sm:w-full">
                            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                <div class="sm:flex sm:items-start">
                                    <div class="m-7 text-center sm:text-left">
                                        <h2 class="text-2xl mb-7 leading-normal font-medium text-gray-900"
                                            id="modal-title">
                                            Modifier votre commentaires </h2>
                                        <div class="mt-2">
                                            @csrf
                                            <div class="mb-4">
                                                <label for="desc" class="block text-sm font-medium text-gray-700">
                                                    Commentaire </label>
                                                <div class="mt-1">
                                                    <textarea rows="3" name="comment" id="comment"
                                                        class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md">{{ $comment->comment }}</textarea>
                                                </div>
                                            </div>
                                            <div class="col-span-6 sm:col-span-3 mb-4">
                                                <label for="note"
                                                    class="block text-sm font-medium text-gray-700">Note</label>
                                                <select name="note" id="note"
                                                    class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                                    @for ($i = 1; $i <= 5; $i++)
                                                        <option value="{{ $i }}"
                                                            {{ $comment->note === $i ? 'selected' : '' }}>
                                                            {{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            @csrf
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                                <input type="submit" value="Modifier"
                                    class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4
            py-2 text-base font-medium text-white bg-amber-800 hover:bg-amber-900 focus:outline-none focus:ring-2
            focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm" />
                                <button type="button"
                                    class="closeModale mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                                    Annuler
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</i>


<script type="text/javascript">
    $(document).ready(function() {
        $(`.openModale{{ $comment->id }}`).on('click', function(e) {
            $(`#interestModale{{ $comment->id }}`).removeClass('invisible');
        });
        $('.closeModale').on('click', function(e) {
            $(`#interestModale{{ $comment->id }}`).addClass('invisible');
        });
    });
</script>
