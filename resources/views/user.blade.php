<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Mon profil') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="flex justify-between">
                        <b>
                            <h2 class="text-2xl">Mes informations</h2>
                        </b>
                        <div>
                            <a href="{{ route('updateInfos', ['id' => Auth::user()->id]) }}" type="button" class="closeModal mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                                Mettre à jour mon profil
                            </a>
                            <button type="button" class="focus:outline-none openModalDelete text-white text-sm py-2.5 px-5 ml-5 rounded-md bg-amber-800">Supprimer
                                votre compte</button>
                        </div>
                    </div>
                    </br>
                    <div class="flex">
                        <div class="avatar mr-4 flex aspect-square " style="width:115px;">
                            <img src="{{ asset('/storage/avatar/' . $user->image) }}" alt="{{ $user->name }}" class="object-cover	rounded-full w-full	">
                        </div>
                        <div>
                            <strong>Nom :</strong> {{ Auth::user()->name }}<br>
                            <strong>Mail :</strong> {{ Auth::user()->email }}<br>
                        </div>
                    </div>

                </div>

                <div class="p-6 bg-white border-b border-gray-200">
                    <b>
                        <h2 class="text-2xl">Mes lieux</h2>
                    </b><br>
                    @foreach ($places as $place)
                    <li class="flex justify-between items-center">
                        <div class="flex">

                            <div class="mr-2 openModalDeletePlace{{$place->id }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="#656565" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                                    <line x1="18" y1="6" x2="6" y2="18"></line>
                                    <line x1="6" y1="6" x2="18" y2="18"></line>
                                </svg>
                            </div>
                            <a href="{{ route('showPlace', ['id' => $place->id]) }}"><b>{{ $place->name }}</b> - {{ $place->address }}</a>
                        </div>
                        <a href="{{ route('updatePlace', $place->id) }}" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4
            py-2 text-base font-medium text-white bg-amber-800 hover:bg-amber-900 focus:outline-none focus:ring-2
            focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:my-2 sm:w-auto sm:text-sm">Mettre
                            à jour</a>

                    </li>
                    @include('components.Modal.deletePlace')
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @include('components.Modal.deleteUser')

</x-app-layout>
