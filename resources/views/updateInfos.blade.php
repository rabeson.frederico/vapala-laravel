<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Mettre à jour mon profil') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form action="{{ route('updateInfos', ['id' => Auth::user()->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="mt-2">

                            <div class="mb-4">
                                <div class=" flex aspect-square justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-full" id="previewFile" style="background-repeat: no-repeat; background-size: cover; background-position: center;position:relative;width:115px; background-image:url({{asset('/storage/avatar/'.$infos->image )}})">
                                    <svg class="deleteImage" style="position: absolute; width: 24px; top: 10px; right: 10px; background-color: white;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18">
                                        <g id="a" />
                                        <g id="b">
                                            <g id="c">
                                                <path d="M18,1c0-.48-.38-1-1-1H1C.38,0,0,.52,0,1v16c0,.62,.52,1,1,1H17c.48,0,1-.38,1-1V1ZM9.01,7.93l2.72-2.72c.15-.15,.34-.22,.53-.22,.4,0,.75,.32,.75,.75,0,.19-.07,.38-.22,.53l-2.72,2.72,2.73,2.73c.15,.15,.22,.34,.22,.53,0,.43-.35,.75-.75,.75-.19,0-.38-.07-.53-.22l-2.73-2.73-2.73,2.73c-.15,.15-.34,.22-.53,.22-.4,0-.75-.32-.75-.75,0-.19,.07-.38,.22-.53l2.73-2.73-2.72-2.72c-.15-.15-.22-.34-.22-.53,0-.43,.35-.75,.75-.75,.19,0,.38,.07,.53,.22l2.72,2.72Z" />
                                            </g>
                                        </g>
                                    </svg>
                                    <div class="space-y-1 text-center inputImage invisible">
                                        <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                                            <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                        </svg>
                                        <div class="flex text-sm text-gray-600 ">
                                            <label for="image" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                                <span>Ajouter un avatar</span>
                                                <input id="image" name="image" accept="image/png, image/jpeg" type="file" class="sr-only">
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-6 sm:col-span-3 mb-4">
                                <label for="first-name" class="block text-sm font-medium text-gray-700">Nom</label>
                                <input type="text" name="name" id="first-name" value="{{ $infos->name }}" autocomplete="given-name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            </div>

                            <div class="col-span-6 sm:col-span-3 mb-4">
                                <label for="last-name" class="block text-sm font-medium text-gray-700">Email</label>
                                <input type="text" name="email" id="location" value="{{ $infos->email }}" autocomplete="location" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                            </div>
                        </div>
                        <input type="submit" value="Mettre à jour mon profil" class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4
            py-2 text-base font-medium text-white bg-amber-800 hover:bg-amber-900 focus:outline-none focus:ring-2
            focus:ring-offset-2 focus:ring-red-500  sm:w-auto sm:text-sm" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
