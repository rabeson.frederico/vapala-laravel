@if ($create)
    <div class="mb-4"><br>
        <div class="mx-auto flex aspect-square justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-full"
            id="previewFile"
            style="background-repeat: no-repeat; background-size: cover; background-position: center;position:relative;width:35%">
            <svg class="deleteImage invisible"
                style="position: absolute; width: 24px; top: 10px; right: 10px; background-color: white;"
                xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18">
                <g id="a" />
                <g id="b">
                    <g id="c">
                        <path
                            d="M18,1c0-.48-.38-1-1-1H1C.38,0,0,.52,0,1v16c0,.62,.52,1,1,1H17c.48,0,1-.38,1-1V1ZM9.01,7.93l2.72-2.72c.15-.15,.34-.22,.53-.22,.4,0,.75,.32,.75,.75,0,.19-.07,.38-.22,.53l-2.72,2.72,2.73,2.73c.15,.15,.22,.34,.22,.53,0,.43-.35,.75-.75,.75-.19,0-.38-.07-.53-.22l-2.73-2.73-2.73,2.73c-.15,.15-.34,.22-.53,.22-.4,0-.75-.32-.75-.75,0-.19,.07-.38,.22-.53l2.73-2.73-2.72-2.72c-.15-.15-.22-.34-.22-.53,0-.43,.35-.75,.75-.75,.19,0,.38,.07,.53,.22l2.72,2.72Z" />
                    </g>
                </g>
            </svg>
            <div class="space-y-1 text-center inputImage">
                <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48"
                    aria-hidden="true">
                    <path
                        d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02"
                        stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                </svg>
                <div class="flex text-sm text-gray-600 ">
                    <label for="image"
                        class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                        <span>Ajouter un avatar</span>
                        <input id="image" name="image" accept="image/png, image/jpeg" type="file" class="sr-only"
                            required>
                    </label>
                </div>
            </div>
        </div>
    </div>
@endif

<!-- Name -->
<div>
    <x-label for="name" :value="__('Nom')" />
    @isset($user)
        <x-input id="name" class="block mt-1 w-full" type="text" name="name" value="{{ $user->name }}" required
            autofocus />
    @endisset
    @if ($create)
        <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus />
    @endif
</div>

<!-- Email Address -->
<div class="mt-4">
    <x-label for="email" :value="__('Email')" />
    @isset($user)
        <x-input id="email" class="block mt-1 w-full" type="email" name="email" value="{{ $user->email }}" required />
    @endisset
    @if ($create)
        <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
    @endif
</div>

@can('is_admin')
    @isset($user)
        <div class="mt-4">
            <label for="is_banned" class="inline-flex items-center mt-3">
                <input type="checkbox" name="is_banned" class="switch-input" value="1"
                    {{ old('is_banned') ? 'checked="checked"' : '' }} @if ($user->is_banned) checked @endif /><span
                    class="ml-4 text-red-500">Bannir</span>
            </label>
        </div>
    @endisset
@endcan

<!-- Password -->
@if ($create)
    <div class="
                mt-4">
        <x-label for="password" :value="__('Mot de passe')" />
        <x-input id="password" class="block mt-1 w-full" type="password" name="password" required
            autocomplete="new-password" />
    </div>
@endif


<div class="mt-4 text-sm">
    @foreach ($roles as $role)
        <label class="inline-flex items-center mt-3">
            <input type="checkbox" class="form-checkbox h-5 w-5 text-green-600" name="roles[]"
                value="{{ $role->id }}" id="{{ $role->name }}"
                @isset($user) @if (in_array($role->id, $user->roles->pluck('id')->toArray()))
                    checked @endif
            @endisset>
        <span class="ml-2 text-gray-700">{{ $role->name }}</span>
    </label>
@endforeach
</div>



<div class="flex items-center justify-end mt-4">
<a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('admin.users.index') }}">
    {{ __('Annuler') }}
</a>

@if ($create)
    <x-button class="ml-4">
        {{ __('Ajouter utilisateur') }}
    </x-button>
@else
    <x-button class="ml-4">
        {{ __('Confimer la modification') }}
    </x-button>
@endif
</div>
