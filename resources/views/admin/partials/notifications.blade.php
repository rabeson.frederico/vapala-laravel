<!-- component -->
<!-- notificatoin box 1 -->
{{-- <div class="shadow-lg rounded-lg bg-white mx-auto m-8 p-4 notification-box">
    <div class="text-sm pb-2">
        Notification Title
        <span class="float-right">
            <svg class="fill-current text-gray-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="22"
                height="22">
                <path class="heroicon-ui"
                    d="M16.24 14.83a1 1 0 0 1-1.41 1.41L12 13.41l-2.83 2.83a1 1 0 0 1-1.41-1.41L10.59 12 7.76 9.17a1 1 0 0 1 1.41-1.41L12 10.59l2.83-2.83a1 1 0 0 1 1.41 1.41L13.41 12l2.83 2.83z" />
            </svg>
        </span>
    </div>
    <div class="text-sm text-gray-600  tracking-tight ">
        I will never close automatically. This is a purposely very very long
        description that has many many characters and words.
    </div>
</div> --}}
<!-- notificatoin box 2 -->
@if (session('success'))
    <div class="notification shadow-lg rounded-lg bg-white mx-auto m-8 p-4 notification-box flex">
        <div class="pr-2">
            <svg class="fill-current text-green-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="22"
                height="22">
                <path class="heroicon-ui"
                    d="M12 22a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm-3.54-4.46a1 1 0 0 1 1.42-1.42 3 3 0 0 0 4.24 0 1 1 0 0 1 1.42 1.42 5 5 0 0 1-7.08 0zM9 11a1 1 0 1 1 0-2 1 1 0 0 1 0 2zm6 0a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" />
            </svg>
        </div>
        <div>
            <div class="text-sm pb-2">
                {{ __('Opération effectuer avec success') }}
                <span onclick="close(event)" class="cross-icon-notif float-right cursor-pointer">
                    <svg class="fill-current text-gray-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                        width="22" height="22">
                        <path class="heroicon-ui"
                            d="M16.24 14.83a1 1 0 0 1-1.41 1.41L12 13.41l-2.83 2.83a1 1 0 0 1-1.41-1.41L10.59 12 7.76 9.17a1 1 0 0 1 1.41-1.41L12 10.59l2.83-2.83a1 1 0 0 1 1.41 1.41L13.41 12l2.83 2.83z" />
                    </svg>
                </span>
            </div>
            <div class="text-sm text-green-600  tracking-tight ">
                {{ session('success') }}
            </div>
        </div>
    </div>
@endif
<!-- notificatoin box 3 -->
@if (session('error'))
    <div class="notification shadow-lg rounded-lg bg-white mx-auto m-8 p-4 notification-box flex">
        <div class="pr-2">
            <svg class="fill-current text-red-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                height="24">
                <path class="heroicon-ui"
                    d="M12 22a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm-3.54-4.54a5 5 0 0 1 7.08 0 1 1 0 0 1-1.42 1.42 3 3 0 0 0-4.24 0 1 1 0 0 1-1.42-1.42zM9 11a1 1 0 1 1 0-2 1 1 0 0 1 0 2zm6 0a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" />
            </svg>
        </div>
        <div>
            <div class="text-sm pb-2">
                {{ __('Un problème est survenu mec !!!! attention !! ') }}
                <span class="float-right cross-icon-notif cursor-pointer">
                    <svg class="fill-current text-gray-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                        width="22" height="22">
                        <path class="heroicon-ui"
                            d="M16.24 14.83a1 1 0 0 1-1.41 1.41L12 13.41l-2.83 2.83a1 1 0 0 1-1.41-1.41L10.59 12 7.76 9.17a1 1 0 0 1 1.41-1.41L12 10.59l2.83-2.83a1 1 0 0 1 1.41 1.41L13.41 12l2.83 2.83z" />
                    </svg>
                </span>
            </div>
            <div class="text-sm text-red-600  tracking-tight ">
                {{ session('error') }}
            </div>
        </div>
    </div>
@endif
<!-- notificatoin box 4 -->
@if (session('warning'))
    <div class="shadow-lg notification rounded-lg bg-white mx-auto m-8 p-4 notification-box flex">
        <div class="pr-2">
            <svg class="fill-current text-orange-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24"
                height="24">
                <path class="heroicon-ui"
                    d="M12 2a10 10 0 1 1 0 20 10 10 0 0 1 0-20zm0 2a8 8 0 1 0 0 16 8 8 0 0 0 0-16zm0 9a1 1 0 0 1-1-1V8a1 1 0 0 1 2 0v4a1 1 0 0 1-1 1zm0 4a1 1 0 1 1 0-2 1 1 0 0 1 0 2z" />
            </svg>
        </div>
        <div>
            <div class="text-sm pb-2">
                {{ __('Alerte !! Alerte!!') }}
                <span class="float-right cross-icon-notif cursor-pointer">
                    <svg class="fill-current text-gray-600" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"
                        width="22" height="22">
                        <path class="heroicon-ui"
                            d="M16.24 14.83a1 1 0 0 1-1.41 1.41L12 13.41l-2.83 2.83a1 1 0 0 1-1.41-1.41L10.59 12 7.76 9.17a1 1 0 0 1 1.41-1.41L12 10.59l2.83-2.83a1 1 0 0 1 1.41 1.41L13.41 12l2.83 2.83z" />
                    </svg>
                </span>
            </div>
            <div class="text-sm text-orange-600  tracking-tight ">
                {{ session('warning') }}
            </div>
        </div>
    </div>
@endif

<style>
    .notification-box {
        width: 20rem;
    }

    .hide {
        display: none;
    }
</style>

<script defer>
    const closeIcon = document.querySelector('.cross-icon-notif ');
    const divNotification = document.querySelector('.notification');
    const close = (event) => {
        divNotification.style.display = "none";
    }
    if (closeIcon) {
        closeIcon.onclick = close;
    }
</script>
