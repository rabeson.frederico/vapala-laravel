<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold cursor-pointer text-xl text-gray-800 leading-tight">
            <a href="{{ route('admin.users.index') }}"> {{ __('Tableau de bord d\'administration') }}</a>
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200 flex justify-between full-width items-center">
                    <h1>Listes des Utilisateurs</h1>
                    @can('is_admin')
                        <a class="text-white bg-green-600 cursor-pointer hover:bg-green-400 rounded border-none py-1 px-2"
                            href="{{ route('admin.users.create') }}" role="button">Créer un nouvel
                            utilisateur</a>
                    @endcan
                    @can('is_manager')
                        <a class="text-gray-300 cursor-pointer rounded border-none py-1 px-2"
                            href="{{ route('admin.users.create') }}" role="button">Créer un nouvel
                            utilisateur</a>
                    @endcan
                </div>
                <div class="text-gray-900 bg-gray-200">
                    <div class="px-3 py-4 flex justify-center flex-col">
                        <table class="w-full text-md bg-white shadow-md rounded mb-4">
                            <tbody>
                                <tr class="border-b">
                                    <th class="text-left p-3 px-5">n°Identifiant</th>
                                    <th class="text-left p-3 px-5">Prénom Nom</th>
                                    <th class="text-left p-3 px-5">Email</th>
                                    <th class="text-center p-3 px-5">Statut</th>
                                    <th class="text-center p-3 px-5">Rôles</th>
                                    <th class="text-left p-3 px-5">Actions</th>
                                    <th></th>
                                </tr>
                                @foreach ($users as $user)
                                    <tr class="border-b hover:bg-orange-100 bg-gray-100 py-4">
                                        <td class="text-center p-3 px-5">{{ $user->id }}_vpl</td>
                                        <td class="p-3 px-5">{{ $user->name }}</td>
                                        <td class="p-3 px-5">{{ $user->email }} </td>
                                        <td class="p-3 px-5">
                                            @if ($user->is_banned)
                                                <p
                                                    class="text-sm bg-red-500 hover:bg-red-700 text-white py-1 px-2 rounded text-center">
                                                    banni
                                                </p>
                                            @else
                                                <p class=" text-gray-600 text-center py-1 px-2">-</p>
                                            @endif
                                        </td>
                                        <td class="p-3 px-5">
                                            @foreach ($roles as $role)
                                                <div class="text-sm text-center">
                                                    @if (in_array($role->id, $user->roles->pluck('id')->toArray()))
                                                        {{ $role->name }}
                                                    @endif
                                                </div>
                                            @endforeach
                                        </td>
                                        <td class="p-3 px-5 flex justify-start">
                                            <a href="{{ route('admin.users.edit', $user->id) }}" role="button"
                                                class=" text-blue-600 text-sm py-1 px-2 rounded focus:outline-none focus:shadow-outline">modifier</a>
                                            @can('is_admin')
                                                <button type="button"
                                                    class="mr-3 text-red-800 text-sm  py-1 px-2 rounded focus:outline-none focus:shadow-outline"
                                                    onclick="event.preventDefault();document.getElementById('delete-user-form-{{ $user->id }}').submit()">supprimer</button>
                                            @endcan
                                            @can('is_manager')
                                                <button type="button"
                                                    class="mr-3 text-gray-300 text-sm  py-1 px-2 rounded focus:outline-none focus:shadow-outline"
                                                    onclick="event.preventDefault();document.getElementById('delete-user-form-{{ $user->id }}').submit()">supprimer</button>
                                            @endcan
                                            <form id="delete-user-form-{{ $user->id }}"
                                                action="{{ route('admin.users.destroy', $user->id) }}" method="POST"
                                                style="display: none">
                                                @csrf
                                                @method('DELETE')
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="user-pagination full-width flex justify-center items-center">
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>



<script>
    const paginationInfo = document.querySelector('.user-pagination nav :nth-child(2) div');
    paginationInfo.firstElementChild.style.display = "none";
</script>
