<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>


<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold cursor-pointer text-xl text-gray-800 leading-tight">
            <a href="{{ route('admin.users.index') }}"> {{ __('Tableau de bord d\'administration') }}</a>
        </h2>

        <p class="text-blue-400">
            {{ __('Mise à jour de l\'utilisateur') }} : {{ $user->id }}
        </p>
    </x-slot>

    <x-guest-layout>
        <x-auth-card class="full-width">
            <x-slot name="logo">
                <a href="/">
                    <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
                </a>
            </x-slot>

            <!-- Validation Errors -->
            <x-auth-validation-errors class="mb-4" :errors="$errors" />

            <form method="POST" action="{{ route('admin.users.update', $user->id) }}" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                @include('admin.partials.form', ['create' => false])
            </form>
        </x-auth-card>
    </x-guest-layout>
</x-app-layout>

<script type="text/javascript">
    $(document).ready(function() {

        $("input[type=file]").on('change', function(input) {
            var file = $("input[type=file]").get(0).files[0];
            if (file) {
                var reader = new FileReader();
                reader.onload = function() {

                    $("#previewFile").css('background-image', "url(" + reader.result + ")");
                    $('.inputImage').addClass('invisible');
                    $('.deleteImage').removeClass('invisible');
                }
                reader.readAsDataURL(file);
            }
        });

        $(".deleteImage").on('click', function() {
            $("#previewFile").css('background-image', "none");
            $('.inputImage').removeClass('invisible');
            $('.deleteImage').addClass('invisible');
            $('#image').val("");
        });
    });
</script>
