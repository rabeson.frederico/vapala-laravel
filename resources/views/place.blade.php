<style>
    .stars {
        --percent: calc(var(--rating) / 5 * 100%);
        display: inline-block;
        font-size: 16px;
        opacity: 1;
    }

    .Stars::before {
        content: "●●●●●";
        letter-spacing: 1px;
        background: linear-gradient(90deg,
                #92400E var(--percent),
                #E5E7EB var(--percent));
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
    }
</style>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('') }}
        </h2>
    </x-slot>

    <div class="py-12" id="test">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">

                    <div class="flex gap-5">
                        <img src="{{ asset('/storage/images/' . $place->image) }}" alt="{{ $place->name }}"
                            class="" style="width:50%;aspect-ratio:16/9;object-fit:cover">
                        <div class="w-full">
                            <div class="flex justify-between items-center">
                                <b>
                                    <h1 class="text-[38px]">{{ $place->name }}</h1>
                                </b>
                                <div style=" --rating: {{ $average_comment }};" class="stars">

                                </div>
                            </div>
                            <span
                                class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
                                {{ $place->category->name }}</span>
                            <div class="flex gap-3 mt-5 mb-4">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                    fill="none" stroke="#92400E" stroke-width="2" stroke-linecap="round"
                                    stroke-linejoin="round">
                                    <path
                                        d="M12 22s-8-4.5-8-11.8A8 8 0 0 1 12 2a8 8 0 0 1 8 8.2c0 7.3-8 11.8-8 11.8z" />
                                    <circle cx="12" cy="10" r="3" />
                                </svg>
                                {{ $place->address }}
                            </div>
                            <div class="mb-8 mt-7">
                                <strong>Description :</strong> <br>{{ $place->description }}<br>
                            </div>

                            <div class=" rounded shadow-lg  bg-gray-200 ">
                                <div class="px-6 py-4">
                                    <div class="font-bold text-xl mb-2">Infos pratiques</div>
                                    <div class="flex gap-3">
                                        Prix moyen : {{ $place->avg_price }}€
                                    </div>
                                    @if ($place->website !== 'NULL')
                                        <div class="flex gap-3">
                                            <a style="color:#92400E" target="_blank" href="{{ $place->website }}">Site
                                                internet</a>
                                        </div>
                                    @endif
                                    @if ($place->phone !== 'NULL')
                                        <div class="flex gap-3">
                                            <a style="color:#92400E" href="tel:{{ $place->phone }}">
                                                {{ $place->phone }}</a>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />

                    <ul>
                        <h2 class="font-bold text-xl mb-2">Commentaires : </h2>
                        @foreach ($comments as $comment)
                            <li class="flex justify-between mb-6 mt-3">
                                <div class="flex flex-col">
                                    <div style=" --rating: {{ $comment->note }};" class="stars">
                                    </div>
                                    {{ $comment->comment }}
                                </div>
                                <div>
                                    @if ($comment->user_id == Auth::id())
                                        <a href="{{ route('deleteComment', $comment->id) }}" type="button"
                                            class="closeModal mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">
                                            Supprimer
                                        </a>
                                        <x-modalEditComponent :comment="$comment" :place="$place->id"
                                            data-taget="#ModalEdit" />
                                    @endif
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <br>


                    <div class=" rounded shadow-2xl">
                        <div class="px-6 py-4">
                            <div class="font-bold text-xl mb-2">Ajouter un commentaire</div>

                            <form method="POST" action="{{ route('postComment', $place->id) }}">
                                <div class="flex">
                                    <div class="col-span-6 sm:col-span-3 mb-4 w-full">
                                        <label for="first-name"
                                            class="block text-sm font-medium text-gray-700">Commentaire</label>
                                        <input type="text" name="comment"
                                            class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md" />
                                    </div>
                                    @error('comment')
                                        <div class="alert-danger">Veuillez remplir le commentaire</div><br />
                                    @enderror
                                    <div class="col-span-6 sm:col-span-3 mb-4 ml-3 w-20">
                                        <label for="note" class="block text-sm font-medium text-gray-700">Note</label>
                                        <select name="note" id="note"
                                            class=" mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                            @for ($i = 1; $i <= 5; $i++)
                                                <option value="{{ $i }}">
                                                    {{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                @csrf
                                <input value="Poster"
                                    class="focus:outline-none openModalDelete text-white text-base py-2.5 px-5 ml-auto rounded-md bg-amber-800"
                                    type="submit">

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
