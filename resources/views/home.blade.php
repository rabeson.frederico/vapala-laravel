<x-app-layout>
    <x-slot name="header">
        <h1 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tous les lieux') }}
        </h1>
    </x-slot>
    @livewireStyles

    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        @if (session('placeAlreadyExist'))
            <h3>La place que vous avez décidé d'ajouter existe déjà. <a
                    href="http://vapala.local/place/{{ session('placeAlreadyExist') }}">Cliquer ici !</a></h3>
        @endif
    </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                @livewire('places-list')
            </div>
        </div>
    </div>
</x-app-layout>

@livewireScripts
