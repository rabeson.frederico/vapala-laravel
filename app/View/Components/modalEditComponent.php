<?php

namespace App\View\Components;

use Illuminate\View\Component;

class modalEditComponent extends Component {

    public $comment;
    public $place;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($comment, $place){
        $this->comment = $comment;
        $this->place = $place;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.modal-edit-component');
    }
}
