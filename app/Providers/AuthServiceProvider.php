<?php

namespace App\Providers;

use App\Models\Comment;
use App\Models\User;
use App\Policies\CommentPolicy;
use App\Policies\PlacePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        Place::class => PlacePolicy::class,
        Comment::class =>  CommentPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is_manager', function (User $user) {
            return $user->hasAnyRole('manager');
        });

        Gate::define('is_admin', function (User $user) {
            return $user->hasAnyRole('admin');
        });

        Gate::define('is_banned', function (User $user) {
            return $user->isBanned() ? false : true;
        });

        Gate::define('is_tech', function (User $user) {
            return $user->hasAnyRole('tech');
        });
    }
}
