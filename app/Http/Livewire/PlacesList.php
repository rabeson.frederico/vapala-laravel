<?php

namespace App\Http\Livewire;

use App\Models\Categorie;
use App\Models\Place;
use Livewire\Component;
use Livewire\WithPagination;

class PlacesList extends Component
{
    use WithPagination;

    public $byCat = '';
    public $query;
    public $perPage = 6;

    public function render()
    {
        if ($this->byCat != null) {
            $places = Place::where([
                ['name', 'like', '%' . $this->query . '%'],
                ['cat_id', $this->byCat]
            ])
                ->paginate($this->perPage);
        } else {
            $places = Place::where('name', 'like', '%' . $this->query . '%')->paginate($this->perPage);
        }


        $categories = Categorie::all();

        return view('livewire.places-list', ['places' => $places, 'categories' => $categories]);
    }
}
