<?php

namespace App\Http\Middleware;

use App\Models\Place;
use Closure;
use Illuminate\Http\Request;

class CheckPlaces
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next){
        $placeAll = Place::paginate(50);
        foreach($placeAll as $place){
            if($request->address == $place->address && $request->name == $place->name){
                // return response()->view('home', ['placeAlreadyExist'=> $place]);
                $request->session()->flash('placeAlreadyExist', $place->id);
                return redirect()->route('home');
            }
        }
        return $next($request);
    }
}
