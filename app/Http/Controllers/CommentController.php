<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    // public function __construct()
    // {
    //     $this->authorizeResource(Comment::class, 'Comment');
    // }

    public function postComment(Request $request, $id)
    {
        // $this->authorize('update', [$request->user(), Comment::class]);

        $request->validate([
            'comment' => 'required',
            'note' => 'required',
        ]);
        $comment = new Comment();
        $comment->comment = $request->comment;
        $comment->user_id = Auth::id();
        $comment->place_id = $id;
        $comment->note = $request->note;
        $comment->save();
        return back();
    }

    public function deleteComment($id)
    {
        // $this->authorize('delete', [User::class, Comment::class]);

        $comment = Comment::findOrFail($id);
        if (Auth::id() != $comment->user_id) {
            abort(403);
        }
        $comment->delete();
        return back();
    }

    public function saveComment(Request $request, $id, $idPlace)
    {
        // $this->authorize('create', User::class);

        $comment = Comment::findOrFail($id);
        if (Auth::id() != $comment->user_id) {
            abort(403);
        }
        $comment->user_id = Auth::id();
        $comment->place_id = $idPlace;
        (empty($request->comment)) ? $comment->comment = $comment->comment : $comment->comment = $request->comment;
        $comment->note = $request->note;
        $comment->save();
        return back();
    }

    public function updateComment(Request $request)
    {
        // $this->authorize('update', [User::class, Comment::class]);

        $comment = Comment::findOrFail($request->id);
        if (Auth::id() != $comment->user_id) {
            abort(403);
        }
        return back();
    }
}
