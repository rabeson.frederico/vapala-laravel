<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Place;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PlaceController extends Controller
{

    // public function __construct()
    // {
    //     $this->authorizeResource(Place::class, 'place');
    // }

    public function index()
    {

        $places = Place::paginate(50);
        return view('home', ['places' => $places]);
    }

    public function postPlace(Request $request)
    { // injection de dépendant
        if ($request->user()->cannot('create', Place::class)) {
            abort(403);
        }
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'description' => 'required',
            'price' => 'required',
        ]);

        $places = new Place();
        $places->name = $request->name;
        $places->address = $request->address;
        $places->description = $request->description;
        $places->avg_price = $request->price;
        $places->cat_id = $request->categories;
        $places->note = $request->note;
        $places->user_id = Auth::id();

        (empty($request->phone)) ? $places->phone = 'NULL' : $places->phone = $request->phone;
        (empty($request->website)) ? $places->website = 'NULL' : $places->website = $request->website;

        $filename = $request->image->getClientOriginalName();
        $places->image = $filename;
        $request->image->storeAs('images', $filename, 'public');

        // $request->session()->flash('success', 'Message Bien envoyé !');
        // var_dump($places);

        $places->save();

        return redirect()->route('home');
    }

    public function showPlace(Request $request)
    {
        // $this->authorize('view', [User::class, Place::class]);

        $comments = DB::table('comments')->where('place_id', $request->id)->get();
        $place = Place::findOrFail($request->id);
        $totalScore = DB::table('comments')->where('place_id', $request->id)->sum('note');
        if ($totalScore) {
            $count = $comments->count();
            $averageComment = ($totalScore + $place->note)  / ($count + 1);
            return view(
                'place',
                [
                    'place' => $place,
                    'comments' => $comments,
                    'average_comment' => $averageComment
                ]
            );
        }
        return view(
            'place',
            [
                'place' => $place,
                'comments' => $comments,
                'average_comment' => $place->note
            ]
        );
    }

    public function deletePlace($id)
    { // injection de dépendant
        // dd($id);
        // $this->authorize('delete',  [User::class, Place::class]);

        $place = Place::find($id);
        if (Auth::id() != $place->user_id) {
            abort(404);
        }
        $place->delete();

        return back();
    }

    public function updatePlace(Request $request)
    { // injection de dépendant
        // dd($id);
        // $this->authorize('update',  [User::class, Place::class]);

        $place = Place::find($request->id);

        if (Auth::id() != $place->user_id) {
            abort(403);
        }
        return view('updatePlace', ['place' => $place]);
    }

    public function savePlace(Request $request)
    { // injection de dépendant
        $place = Place::find($request->id);
        if (Auth::id() != $place->user_id) {
            abort(404);
        }
        $place->name = $request->name;
        $place->description = $request->description;
        $place->avg_price = $request->price;
        $place->address = $request->address;
        $place->note = $request->note;
        $place->cat_id = $request->categories;
        $place->user_id = Auth::id();
        (empty($request->phone)) ? $place->phone = 'NULL' : $place->phone = $request->phone;
        (empty($request->website)) ? $place->website = 'NULL' : $place->website = $request->website;


        $place->user_id = Auth::id();


        if ($request->hasFile('image')) {
            $filename = $request->image->getClientOriginalName();
            $request->image->storeAs('images', $filename, 'public');
        } else {
            $filename = $place->image;
        }

        $place->image = $filename;
        $place->save();

        return redirect()->route('home');
    }
}
