<?php

namespace App\Http\Controllers;

use App\Models\Place;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function showUser(Request $request)
    {
        $user = User::findOrFail($request->id);
        $places = Place::where('user_id', '=', Auth::id())->get();

        return view('user', ['user' => $user, 'places' => $places]);
    }

    public function updateInfos(Request $request) // injection de dépendant
    {
        // dd($id);
        $infos = User::find($request->id);
        return view('updateInfos', ['infos' => $infos]);
    }

    public function saveInfos(Request $request) // injection de dépendant
    {
        $infos = User::find($request->id);

        if ($request->hasFile('image')) {
            $filename = $request->image->getClientOriginalName();
            $request->image->storeAs('avatar', $filename, 'public');
        } else {
            $filename = $infos->image;
        }

        $infos->id = Auth::id();
        $infos->update(
            [
                $infos->image = $filename,
                $infos->name = $request->name,
                $infos->email = $request->email
            ]
        );

        return redirect()->route('showUser', ['id' => Auth::user()->id]);
    }

    public function destroy()
    {
        User::destroy(Auth::id());
        return redirect()->route('home');
    }
}
