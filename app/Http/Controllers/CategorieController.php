<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategorieController extends Controller
{
    public function save_post(Request $request)
    {
        $categorie = new Categorie;
        $categorie->name = $request->name;
        $categorie->id = Auth::id();
        $categorie->save();

        return redirect()->route('dashboard');
    }
}