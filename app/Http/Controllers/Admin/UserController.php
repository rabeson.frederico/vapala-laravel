<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Models\Role;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class UserController extends Controller
{

    // public function __construct()
    // {
    //     $this->authorizeResource(User::class);
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $roles = Role::all();

        $users = User::paginate(10);

        if (Gate::allows('is_admin') || Gate::allows('is_manager')) {
            return view('admin.users.index', ['users' => $users, 'roles' => $roles]);
        }

        return redirect(RouteServiceProvider::HOME);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (Gate::check('is_manager')) {
            $request->session()->flash('warning', 'Vous n\'êtes pas autorisé à effectuer cette action. Contacter un Administrateur');
            return redirect(route('admin.users.index'));
        }

        Gate::authorize('is_admin');

        $roles = Role::all();

        return view('admin.users.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        if (Gate::check('is_manager')) {
            $request->session()->flash('warning', 'Vous n\'êtes pas autorisé à effectuer cette action. Contacter un Administrateur');
            return redirect(route('admin.users.index'));
        }

        Gate::authorize('is_admin');

        $request->validated();

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'image' => "rien",
            'password' => Hash::make($request->password),
        ]);

        $avatar = $request->image->getClientOriginalName();

        $request->image->storeAs('avatar', $avatar, 'public');

        $user->update(['image' => $avatar]);

        $user->roles()->sync($request->roles);

        $request->session()->flash('success', 'L\'utilisateur a bien été créé et va bientôt regréter');

        return redirect(route('admin.users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(string $id)
    {

        $user = User::findOrFail($id);

        $roles = Role::all();

        return view('admin.users.edit', ['user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $id)
    {
        $user = User::find($id);
        // $user = User::findOrFail($id);

        if (!$user) {
            $request->session()->flash('error', 'Vous ne pouvez pas modifier les informations de cet utilisateur');
            return redirect(route('admin.users.index'));
        }

        $user->update($request->except(['_token', 'roles']));

        $user->roles()->sync($request->roles);

        $user->is_banned = $request->has('is_banned');

        $user->save();

        $request->session()->flash('success', 'Les informations de l\'utilisateur ont bien été misa à jour frère');

        return redirect(route('admin.users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, string $id)
    {

        if (Gate::check('is_manager')) {
            $request->session()->flash('warning', 'Vous n\'êtes pas autorisé à effectuer cette action. Contacter un Administrateur');
            return redirect(route('admin.users.index'));
        }

        Gate::authorize('is_admin');

        $user = User::findOrFail($id);

        $user->roles()->detach();

        $user->destroy($id);

        $request->session()->flash('success', 'L\'utilisateur a été supprimé avec succès, rentre chez toi brooo');

        return redirect(route('admin.users.index'));
    }
}
