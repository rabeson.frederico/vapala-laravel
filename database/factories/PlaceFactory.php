<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Place>
 */
class PlaceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(50),
            'cat_id' => $this->faker->numberBetween(0, 10),
            'user_id' => $this->faker->numberBetween(0, 30),
            'description' => $this->faker->text(100),
            'avg_price' => $this->faker->numberBetween(50, 100),
            'image' => $this->faker->imageUrl(640, 480, 'animals', true),
            'note' => $this->faker->numberBetween(0, 5),
            'phone' => $this->faker->phoneNumber(),
            'website' => $this->faker->url(),
            'address' => $this->faker->address()
        ];
    }
}
