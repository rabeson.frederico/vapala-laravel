<?php

namespace Database\Seeders;

use App\Models\Categorie;
use App\Models\Place;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PlaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        $categories = Categorie::all();

        Place::all()->each(function ($place) use ($users) {
            $place->user()->attach(
                $users->random(1)->pluck('id')
            );
        });

        Place::all()->each(function ($place) use ($categories) {
            $place->category()->attach(
                $categories->random(1)->pluck('id')
            );
        });

        Place::factory()->times(10)->create();
    }
}
